import React from 'react';
import './App.css';
import SearchBar from "../SearchBar/SearchBar";
import SearchResults from "../SearchResults/SearchResults";
import Playlist from "../Playlist/Playlist";

class App extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            searchResults: [
                {
                    name: 'Hello',
                    artist: 'Selena',
                    album: 'New World',
                    id: 1,
                },
                {
                    name: 'Hello1',
                    artist: 'Selena1',
                    album: 'New World1',
                    id: 2,
                },
                {
                    name: 'Hello2',
                    artist: 'Selena2',
                    album: 'New World2',
                    id: 3,
                }
            ],
            playlistName: '',
            playlistTracks: [
                {
                    name: 'Hello5',
                    artist: 'Selena',
                    album: 'New World',
                    id: 4,
                },
                {
                    name: 'Hello16',
                    artist: 'Selena1',
                    album: 'New World1',
                    id: 3,
                },
                {
                    name: 'Hello29',
                    artist: 'Selena2',
                    album: 'New World2',
                    id: 6,
                }
            ]
        }
    }

    // Add a songs from searchList to playList
    addTrack = track => {
        const tracks = this.state.playlistTracks;
        // check the id's matching condition
        if(tracks.find(savedTrack => savedTrack.id === track.id)) {
            return;
        } else {
            tracks.push(track)
        }
        this.setState({
            playlistTracks: tracks
        })
    };

    // remove a item from the playList
    removeTrack = (track) => {
        let tracks = this.state.playlistTracks;
        tracks = tracks.filter(savedTrack => savedTrack.id !== track.id);
        this.setState({
            playlistTracks: tracks
        })
    };

    // update the platList name
    updatePlaylistName = (name) => {
        this.setState({
            playlistName: name
        })
    };

    render() {
        const {searchResults, playlistName, playlistTracks} = this.state;
        return (
            <div>
                <h1>Ja<span className="highlight">mmm</span>ing</h1>
                <div className="App">
                    { /* !-- Add a SearchBar component -- */}
                    <SearchBar />
                    <div className="App-playlist">
                        {/* !-- Add a SearchResults component --*/}
                        <SearchResults
                            searchResults = {searchResults}
                            onAdd={this.addTrack}
                        />
                        {/* !-- Add a Playlist component --*/}
                        <Playlist
                            playlistName = {playlistName}
                            playlistTracks = {playlistTracks}
                            onRemove={this.removeTrack}
                            onNameChange = {this.updatePlaylistName}
                        />
                    </div>
                </div>
            </div>
        )
    }
}

export default App;
