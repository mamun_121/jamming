import React from 'react';
import './Playlist.css';
import TrackList from "../TrackList/TrackList";

class Playlist extends React.Component{
    handleNameChange = (event) => {
        this.props.onNameChange(event.target.value);
    };
    render() {
        const {playlistTracks, onRemove} = this.props;
        return(
            <div className="Playlist">
                <input defaultValue="New Playlist" onChange={this.handleNameChange}/>
                {/* !-- Add a TrackList component --*/}
                <TrackList
                    tracks={playlistTracks}
                    onRemove={onRemove}
                    isRemoval={true}
                />
                <button className="Playlist-save">SAVE TO SPOTIFY</button>
            </div>
        );
    }
}

export default Playlist;