import React from 'react';
import './Track.css';

class Track extends React.Component{
    // displays a <button> element with - as its content if the isRemoval property is true,
    // and a + <button> element if the isRemoval property is false
    renderAction() {
        if(this.props.isRemoval) {
            return <button
                className='Track-action'
                onClick={()=> this.props.onRemove(this.props.track)}>-</button>
        }else {
            return <button
                className='Track-action'
                onClick={()=> this.props.onAdd(this.props.track)}>+</button>
        }
    }
    // addtrack = () => {
    //     const {track, onAdd} = this.props;
    //     onAdd(track);
    // };

    render() {
        const {album, artist, name} = this.props.track;
        return(
            <div className="Track">
                <div className="Track-information">
                    <h3>{name}</h3>
                    <p>{artist} | {album}</p>
                </div>
                {/*!-- + or - will go here --*/}
                {this.renderAction()}
            </div>
        );
    }
}

export default Track;