import React from 'react';
import './TrackList.css';
import Track from "../Track/Track";

class TrackList extends React.Component{
    render() {
        const {tracks, onAdd, onRemove, isRemoval} = this.props;
        return(
            <div className="TrackList">
                {/* !-- You will add a map method that renders a set of Track components  --*/}
                {tracks.map(track => (
                    <Track key={track.id}
                           track={track}
                           onAdd={onAdd}
                           onRemove={onRemove}
                           isRemoval={isRemoval}
                    />
                ))}
            </div>
        );
    }
}

export default TrackList;